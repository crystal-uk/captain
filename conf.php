<?php

return [

  '*' => [
    'hooks' => [
      [ 
        'id' => 1,
        'url' => 'https://api.netlify.com/build_hooks/5d4c0bf2530d014e687af3f9',
        'siteId' => 4,
        'action' => 'netlify',
      ],
      [
        'id' => 2,
        // 'url' => 'https://eng1wh6lhd5sl.x.pipedream.net/',
        'url' => 'https://api.jasmineactive.com/',
        'siteId' => 7,
        'action' => 'netlify',
        // 'title' => 'This one also has a title'
      ],
      [
        'id' => 3,
        'title' => 'This one kills things',
        'url' => 'https://eng1wh6lhd5sl.x.pipedream.net/',
        'action' => 'DELETE',
      ],
      [
        'id' => 4,
        // 'title' => 'This one is broken',
        'url' => 'https://api.jasmineactive.com/',
        'action' => 'POST',
      ],
    ]
  ],

  'staging' => [
    
  ],

];
