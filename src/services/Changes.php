<?php
namespace crystal\captain\services;

use crystal\captain\Captain;
use crystal\captain\models\Hook;
use crystal\captain\records\Change;

use Craft;
use craft\base\Component;

class Changes extends Component {

  private $elements = [
    'craft\elements\GlobalSet', 
    'craft\elements\Category',
    'craft\elements\Entry', 
    // 'craft\elements\Asset', 
    // 'craft\elements\User', 
  ];

	public function content($event) {

    $element = $event->sender;
    $class = get_class($event->sender);
    
    if (!in_array($class, $this->elements)) {
      return false;
    }

    $settings = Captain::$plugin->settings;

    if (
      !in_array($element->siteId, $settings->watched) ||
      in_array($element->section->handle ?? '', $settings->ignored[$element->siteId] ?? [])
    ) {
      return false;
    }
    
    $change = new Change([
      'type' => 'content', 
      'class' => $class, 
      'siteId' => $event->sender->siteId
    ]);
    
    $change->save();
  }

  public function clean() {
    $settings = Captain::$plugin->settings;
    $hooks = Captain::$plugin->hooks->all();

    Change::deleteAll(['not in', 'siteId', $settings->watched]);
    
    foreach ($hooks as $hook) {
      Change::deleteAll(['and', ['siteId' => $hook->siteId], ['<', 'dateUpdated', $hook->trigger->dateUpdated]]);
    }
  }
}
