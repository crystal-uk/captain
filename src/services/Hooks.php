<?php
namespace crystal\captain\services;

use crystal\captain\Captain;
use crystal\captain\models\Hook;

use Craft;
use craft\base\Component;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;


class Hooks extends Component {

	public function send($settings) {

    $hook = new Hook($settings);
    $hook->validate();

    $client = new Client();
    $body = $hook->body ? ['body' => $hook->body] : [];
    
    try {
      $client->request($hook->method, $hook->url, $body);
      $hook->trigger->save();

    } catch (RequestException $error) {
      return [ 'error' => $error ?? '' ];
    }

    Captain::$plugin->changes->clean();

    return [ 'error' => null ];
  }

  public function all($validate = false) {
    return array_map(function ($hook) use ($validate) {
      $model = new Hook($hook);

      if ($validate) {
        $model->validate();
      }

      return $model;
    }, Captain::$plugin->settings->hooks);
  }

  public function badge() {
    return array_reduce($this->all(), function ($carry, $hook) {
      if ($hook->changeCount) {
        $carry++;
      }
      
      return $carry;
    }, 0);
  }

}
