$('.js-captain-refresh').click(function() {
  const { id } = $(this).data();
  const img = $(`#js-captain-status-${id}`);

  img.attr('src', img.attr('src'));
});
