<?php
namespace crystal\captain;

use crystal\captain\Bundle;
use crystal\captain\services\Changes;
use crystal\captain\services\Hooks;
use crystal\captain\models\Settings;

use Craft;
use craft\base\Plugin;
use craft\base\Element;
use craft\services\Plugins;
use craft\web\View;
use craft\web\twig\variables\CraftVariable;

use yii\base\Event;

class Captain extends Plugin {

	public static $plugin;
	public $schemaVersion = '1.0.01';
	public $hasCpSettings = true;
	public $hasCpSection = true;

	public $badgeCount = 0;

	public function init() {
    parent::init();
		self::$plugin = $this;
		
		$this->setComponents([ 
			'hooks' => Hooks::class,
			'changes' => Changes::class,
		]);

		// Hook Events
		Event::on(Element::class, Element::EVENT_AFTER_SAVE, function($event) {
			$this->changes->content($event);
		});
		Event::on(Element::class, Element::EVENT_AFTER_DELETE, function($event) {
			$this->changes->content($event);
		});
		Event::on(Element::class, Element::EVENT_AFTER_MOVE_IN_STRUCTURE, function($event) {
			$this->changes->content($event);
		});

		$this->name = 'Rebuild';

		$this->badgeCount = $this->hooks->badge();

		Event::on(CraftVariable::class, CraftVariable::EVENT_INIT,
      function (Event $event) {
        $variable = $event->sender;
        $variable->set('captain', Variables::class);
      }
		);

		Event::on(View::class, View::EVENT_END_BODY, function(Event $event) {
			$request = Craft::$app->request;
			$user = Craft::$app->getUser();

			if (
				!$request->getIsConsoleRequest() && $request->getIsCpRequest() && 
				!$request->getAcceptsJson() && $user->checkPermission('accessPlugin-captain') &&
				$this->badgeCount && $request->segments[0] !== 'captain'
			) {	
				echo Craft::$app->view->renderTemplate('captain/_modal', ['hooks' => Captain::$plugin->hooks->all(true)]);
			}
		});
	}
	
	public function getCpNavItem() {
    $item = parent::getCpNavItem();
    $item['badgeCount'] = $this->badgeCount;

    return $item;
	}
  
  protected function createSettingsModel() {
    return new Settings();
	}
	
	protected function settingsHtml() {
		$overrides = Craft::$app->getConfig()->getConfigFromFile(strtolower($this->handle));

		if ($post = Craft::$app->getRequest()->getBodyParam('settings')) {
			$settings = new Settings();
			$settings->attributes = Craft::$app->getRequest()->getBodyParam('settings');
			$settings->validate();
		}

		return Craft::$app->getView()->renderTemplate('captain/settings', [
			'settings' => $settings ?? $this->getSettings(),
			'overrides' => array_keys($overrides),
		]);
	}
}
