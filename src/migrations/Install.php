<?php
namespace crystal\captain\migrations;

use craft\db\Migration;


class Install extends Migration {

  private $changes = '{{%captain_changes}}';
  private $triggers = '{{%captain_triggers}}';

  public function safeUp() {

    $this->createTable($this->changes, [
      'id' => $this->primaryKey(),
      'type' => $this->string()->notNull(),
      'class' => $this->string(),
      'siteId' => $this->integer(),

      'dateCreated'  => $this->dateTime()->notNull(),
      'dateUpdated'  => $this->dateTime()->notNull(),
      'uid'          => $this->uid(),
    ]);

    $this->createTable($this->triggers, [
      'id' => $this->primaryKey(),
      'hookId' => $this->integer()->notNull(),

      'dateCreated'  => $this->dateTime()->notNull(),
      'dateUpdated'  => $this->dateTime()->notNull(),
      'uid'          => $this->uid(),
    ]);

  }

  public function safeDown() {

    $this->dropTable($this->changes);
    $this->dropTable($this->triggers);

  }

}