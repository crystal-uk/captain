<?php
namespace crystal\captain\records;

use Craft;
use craft\db\ActiveRecord;


class Change extends ActiveRecord {

  public static function tableName() {
    return '{{%captain_changes}}';
  }

}
