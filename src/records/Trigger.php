<?php
namespace crystal\captain\records;

use Craft;
use craft\db\ActiveRecord;


class Trigger extends ActiveRecord {

  public static function tableName() {
    return '{{%captain_triggers}}';
  }

}
