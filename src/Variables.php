<?php
namespace crystal\captain;

use crystal\captain\Captain;
use crystal\captain\models\Hook;

use Craft;


class Variables {

  public function settings() {
    return Captain::$plugin->settings;
  }

  public function hooks() {
    return Captain::$plugin->hooks->all(true);
  }

}
