<?php
namespace crystal\captain\controllers;

use crystal\captain\Captain;
use crystal\captain\models\Settings;

use Craft;
use craft\web\Controller;


class BuildController extends Controller {

	public function actionOne() {
		$settings = Captain::$plugin->settings;
		$hookId = Craft::$app->getRequest()->getRequiredBodyParam('hookId');

		$hookNo = array_search($hookId, array_column($settings->hooks, 'id'));
		$hook = $settings->hooks[$hookNo];

		$send = Captain::$plugin->hooks->send($hook);

		if ($send['error']) {
			Craft::$app->session->setError('Failed to trigger hook');

			return $this->renderTemplate('captain', [
				'errors' => [$hookNo => $send['error']]
			]);
		}
		
		Craft::$app->session->setNotice('Success');
		return $this->redirectToPostedUrl();
	}
	
}
