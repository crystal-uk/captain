<?php
namespace crystal\captain\models;

use crystal\captain\records\Change;
use crystal\captain\records\Trigger;

use Craft;
use craft\base\Model;
use yii\base\DynamicModel;


class Hook extends Model {

  public $id;
  public $siteId;
  public $title;
  public $url;
  public $action;
  public $body;
  public $status;
  public $ignore;

  private $actionMap = [
    'netlify' => ['method' => 'POST', 'body' => '{}']
  ];
  
  public function rules() {
    return [
      ['url', 'url'],
      ['status', 'string'],
      [['url', 'id'], 'required'],
      ['action', 'in', 'range' => ['netlify', 'GET', 'POST', 'PUT', 'PATCH', 'DELETE']],

      ['action', 'default', 'value' => 'netlify'],
      ['ignore', 'default', 'value' => []],
      ['body', 'default', 'value' => function ($model, $attribute) {
        return $model->actionMap[$model->action]['body'] ?? $model->body;
      }],
    ];
  }

  public function getMethod() {
    return $this->actionMap[$this->action]['method'] ?? $this->action;
  }

  public function getSite() {
    return $this->siteId ? Craft::$app->sites->getSitebyId($this->siteId): null;
  }

  public function getTrigger() {
    return Trigger::findOne(['hookId' => $this->id]) ?? new Trigger(['hookId' => $this->id]);
  }

  public function getChanges() {
    $query = Change::find()->where(['siteId' => $this->siteId]);

    if ($this->trigger->dateUpdated) {
      $query = $query->andWhere(['>', 'dateUpdated', $this->trigger->dateUpdated]);
    }

    return $query;
  }

  public function getChangeCount() {
    return $this->changes->count();
  }

  public function attributeLabels() {
    return [
      'id' => 'ID',
      'url' => 'Target URL',
    ];
  }

}