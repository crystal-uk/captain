<?php
namespace crystal\captain\models;

use crystal\captain\models\Hook;

use craft\base\Model;


class Settings extends Model {
  public $hooks = [];

  public function rules() {
    return [
      [['hooks'], 'validateHooks']
    ];
  }

  public function validateHooks() {
    $max = (int) max(array_column($this->hooks, 'id'));
    
    foreach ($this->hooks as &$hook) {
      if (!$hook['id']) {
        $hook['id'] = $max + 1;
        $max++;
      }

      $model = new Hook($hook);
      $model->validate();

      if ($model->hasErrors()) {
        $flatErrors = [];

        foreach ($model->errors as $key => $errors) {
          $hook[$key] = ['value' => $hook[$key], 'hasErrors' => true];
          $flatErrors = array_merge($flatErrors, $errors);
        }
        
        $this->addErrors(['hooks' => $flatErrors]);
      }
    }
  }

  public function getWatched() {
    return array_filter(array_column($this->hooks, 'siteId'));
  }

  public function getIgnored() {
    return array_reduce($this->hooks, function ($carry, $hook) {
      if ($hook['ignore'] ?? false) {
        $carry[$hook['siteId']] = $hook['ignore'];
      }

      return $carry;
    }, []);
  }

}