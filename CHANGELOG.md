# Captain Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## 1.0.01 - 2018-06-15

### Fixed

- Typo in log

## 1.0.0 - 2018-06-15

### Added

- Settings/Config
- Element Events: Save, Delete, Move
- Webhook tester

## 0.0.1 - 2018-06-07

### Added

- Initial release
